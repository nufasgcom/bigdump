#: -*- coding: utf-8 -*-
"""
	Класс конфигурации для резервного копирования БД с сервера MySQL
"""
import os
import datetime
import time
from datetime import timedelta


class DBParam(object):
	def __init__(self, item):
		try:
			key, param = item
			self._db = key
			self._user = param['DB_USER']
			self._password = param['DB_PASSWORD']
			self._part_size = param['PART_SIZE']
			self._drop_tables= param['DROP_TABLES']
			self._dump_tables = param['DUMP_TABLES']
			self._skip_tables = param['SKIP_TABLES']
			self._prev_backup_sql = param['PREV_BACKUP_SQL']
			self._after_backup_sql = param['AFTER_BACKUP_SQL']
			self._after_restore_sql = param['AFTER_RESTORE_SQL']
		except Exception, e:
			print ('Error read DBParam with error - "{}"'.format(str(e)))


	@property
	def drop_tables(self):
		return self._drop_tables

	@property
	def db(self):
		return self._db

	@property
	def user(self):
		return self._user

	@property
	def password(self):
		return self._password

	@property
	def part_size(self):
		return self._part_size

	@property
	def dump_tables(self):
		return self._dump_tables

	@property
	def skip_tables(self):
		return self._skip_tables


	@property
	def prev_backup_sql(self):
		return self._prev_backup_sql

	@property
	def after_backup_sql(self):
		return self._after_backup_sql

	@property
	def after_restore_sql(self):
		return self._after_restore_sql

class CFG(object):

	def __init__(self, conf):
		#print(conf)
		try:
			self._host = conf['MySQL_HOST']
			self._port = conf['MySQL_PORT']
			self._dir = conf['BACKUP_DIR']
			self._createDB = conf['CREATE_DB']
			self._backup_dt = datetime.datetime.now()
			self._dbparams = []
			self._insertNoSpace = conf['InsertNoSpace']
			self._insertOneLine = conf['InsertOneLine']
			self._save_by_line = conf['SAVE_BY_LINE']
			week_day_num = datetime.datetime.today().isoweekday()
			shedule = conf['SHEDULE']
			db_params_name = shedule[week_day_num-1]
			for param in conf[db_params_name].items():
				self._dbparams.append(DBParam(param))
			self._procDelimetr = conf['PROC_DELIMETR']
			self._DAY_OF_LIFE = conf['DAY_OF_LIFE']
			self._conf = conf  # Исходная конфигурация
			self._file_name = db_params_name+'_'+conf['BACKUP_NAME']
		except Exception, e:
			print ('Error read config with error - "{}"'.format(str(e)))


	@property
	def createDB(self):
		return self._createDB


	@property
	def saveByLine(self):
		return self._save_by_line

	@property
	def DateForRemove(self):
		return (self._backup_dt - timedelta(days=self._DAY_OF_LIFE)).date()

	def removeOldFiles(self):
		files = os.listdir(self._dir)
		files_for_remove = []
		for file_ in files:
			remove_file = '{dir}/{file}'.format(dir=self._dir, file=file_)
			if self.File_For_Remove(remove_file):
				print ('Removed file {del_file}'.format(del_file=remove_file))
				os.remove(remove_file)


	def getDateFromBackupFileName(self, filename):
		try:
			print(filename)
			if os.name == 'nt':
				unix = os.path.getctime(filename)
			else:
				stat = os.stat(filename) 
				unix = stat.st_birthtime
	#		print('{} - {}'.format(unix,type(unix)))
			date = datetime.datetime.fromtimestamp(unix).date()#.strftime('%Y-%m-%d')
	#		print('{} - {}'.format(date,type(date)))
		except ValueError:
			date = self._backup_dt.date()      # Если не удалось идентифицировать то вернем сегодняшнюю дату
		return date

	def File_For_Remove(self, filename):
		fileDate = self.getDateFromBackupFileName(filename)
		if fileDate < self.DateForRemove:
			print('{} Дата {}{} < {}{} сравнение '.format(filename, fileDate, type(fileDate), self.DateForRemove, type(self.DateForRemove)))
			return True
		else:
			return False

	@property
	def procDelimetr(self):
		return self._procDelimetr

	@property
	def insertNoSpace(self):
		return self._insertNoSpace

	@property
	def insertOneLine(self):
		return self._insertOneLine

	def file(self, preffix='', suffix=''):
		return self.dir + '/' + preffix + self.file_name + suffix + self.backup_dt_str

	def sqlfile(self, preffix='', suffix=''):
		return self.file(preffix, suffix) + '.sql'


	def logfile(self, preffix='', suffix=''):
		return self.file(preffix, suffix) + '.log'


	def arcfile(self, preffix='', suffix=''):
		return self.file(preffix, suffix)  + '.zip'

	@property
	def backup_dt(self):
		return self._backup_dt

	@property
	def backup_dt_str(self):
		return self._backup_dt.strftime("%Y%m%d_%H%M%S")

	@property
	def host(self):
		return self._host

	@property
	def port(self):
		return self._port

	@property
	def file_name(self):
		return self._file_name

	@property
	def dir(self):
		return self._dir

	@property
	def dbparams(self):
		return self._dbparams

	@property
	def conf(self):
		return self._conf
