#: -*- coding: utf-8 -*-
"""
    Файл конфигурирования приложения
"""

# список параметров для резервного копирования
configs = [
# Словарь параметров резервного копирования
	{   # Параметры для первого сервера
		'MySQL_HOST': '127.0.0.1',          # IP адресс или DNS имя MySQL сервера
		'MySQL_PORT': 3306,                 # порт MySQL сервера
		'BACKUP_NAME': 'grp_skz',           # корень имени бакапа
		'BACKUP_DIR': 'e:/backups',         # Папка бакапа
		'InsertNoSpace': True,
		'InsertOneLine': False,
		'PROC_DELIMETR': '+-+',             # Делитель для процедур, представлений и функций
		'DAY_OF_LIFE': 15,                  # Сколько дней живет backup (0 - бесконечно)
		'SAVE_BY_LINE': True,               # Записывать скрипты вставки по 1 строке
		'CREATE_DB': False,                 # Создавать ли автоматически БД (предпочитаю создавать в ручную, так большая гибкость, а то редактировать большие файлы (чтоб изменить наименованике БД) не просто)
		'SHEDULE': [
			'INC_BACKUP_DB_PARAMS',  # Для понедельника
			'INC_BACKUP_DB_PARAMS', # Для вторника
			'INC_BACKUP_DB_PARAMS',  # Для среды
			'INC_BACKUP_DB_PARAMS',  # Для четверга
			'INC_BACKUP_DB_PARAMS',  # Для пятницы
			'FULL_BACKUP_DB_PARAMS', # Для субботы
			'INC_BACKUP_DB_PARAMS'   # Для воскресения
		],
		'FULL_BACKUP_DB_PARAMS': {                    # справочник параметров для полного бакапа каждой БД (префикс имени бакапа)
			'grp': {                                  # БД
						'DB_USER': 'grp',             # пользователь БД
						'DB_PASSWORD': 'uhg',         # пароль пользователя
						'PART_SIZE': 100,             # размер в мегабайтах для деления таблицы на части
						'DROP_TABLES': True,          # удалять таблицы перед восстановлением
						'PREV_BACKUP_SQL': [          # SQL скрипты выполняемые перед резервным копированием
							# '',
						],
						'AFTER_BACKUP_SQL': [         # SQL скрипты выполняемые после резервного копирования
							# '',
						],
						'AFTER_RESTORE_SQL': [        # SQL скрипты выполняемые после резервного копирования
							#'delete FROM raw WHERE date(dt)=(SELECT date(MAX(dt)) FROM raw)',
							#'delete FROM diagnosis_journal WHERE date(add_time)=(SELECT date(MAX(add_time)) FROM diagnosis_journal)',
							#'delete FROM alarms WHERE date(al_dt)=(SELECT date(MAX(al_dt)) FROM alarms)',

						],
						'DUMP_TABLES': [              # резервировать указанные таблицы (маска указывается в формате как для LIKE)
							'%',
						],
						'SKIP_TABLES':[               # исключения (для каких таблиц не производить резервное копирование)
							'%tempdump',
						],
					},
			'newskz': {                               # БД
						'DB_USER': 'skz',             # пользователь БД
						'DB_PASSWORD': 'superskz',    # пароль пользователя
						'PART_SIZE': 100,             # размер в мегабайтах для деления таблицы на части
						'DROP_TABLES': True,          # удалять таблицы перед восстановлением
						'PREV_BACKUP_SQL': [          # SQL скрипты выполняемые перед резервным копированием
							# '',
						],
						'AFTER_BACKUP_SQL': [         # SQL скрипты выполняемые после резервного копирования
							# '',
						],
						'AFTER_RESTORE_SQL': [        # SQL скрипты выполняемые после резервного копирования
							# Удаляем данные которые будут конфликтовать с инкрементным бакапом 
							#'delete FROM parameters WHERE date(add_date)=(SELECT date(MAX(add_date)) FROM parameters)',
							#'delete FROM alarms WHERE date(add_date)=(SELECT date(MAX(add_date)) FROM alarms)',

						],
						'DUMP_TABLES': [              # резервировать указанные таблицы
							'%',
						],
						'SKIP_TABLES': [              # исключения (для каких таблиц не производить резервное копирование)
							'%tempdump',
						],
			},
		},
		'INC_BACKUP_DB_PARAMS': {                     # справочник параметров для полноинкрементального бакапа каждой БД
		                                              # инкрементальность создается для больших таблиц  посредством выполнения скриптов создания дополнительных
		                                              # таблиц и копирования в них разростных данных
		                                              # а также исключением из плана бакапа полных таблиц
			'grp': {
				'DB_USER': 'grp',                     # пользователь БД
				'DB_PASSWORD': 'uhg',                 # пароль пользователя
				'PART_SIZE': 100,                     # размер в мегабайтах для деления таблицы на части
				'DROP_TABLES': True,                  # удалять таблицы перед восстановлением
				'PREV_BACKUP_SQL': [                  # SQL скрипты выполняемые перед резервным копированием
						                              # если вдруг остались таблицы с инкрементальными данными то удаляем их
						'DROP TABLE IF EXISTS `inc_raw`;'
					,
						'DROP TABLE IF EXISTS  `inc_diagnosis_journal` ;'
					,
						'DROP TABLE IF EXISTS  `inc_alarms`;'
					,                                 # создаем таблицу для сохранения инкрементальных данных
						'CREATE TABLE `inc_raw` ( \
							`rid` BIGINT(20) UNSIGNED NOT NULL, \
							`dt` DATETIME NULL DEFAULT NULL, \
							`par_id` BIGINT(20) NULL DEFAULT NULL, \
							`value` FLOAT(12) NULL DEFAULT NULL, \
							`max` FLOAT(12) NOT NULL, \
							`min` FLOAT(12) NOT NULL, \
							`alarm` TINYINT(4) NULL DEFAULT NULL, \
							`is_manual` TINYINT(1) NULL DEFAULT "0" \
						) \
						COLLATE="utf8_unicode_ci";'
					,                                 # копируем в таблицу данные за последние сутки начиная с предыдущих суток
						'INSERT INTO `inc_raw` SELECT * FROM `raw` WHERE to_days(dt)<=to_days(now()) and to_days(dt)>to_days(now())-2;'
					,                                 # создаем таблицу для сохранения инкрементальных данных
						"CREATE TABLE `inc_diagnosis_journal` ( \
							`id` BIGINT(20) UNSIGNED NOT NULL, \
							`grp` INT(11) NOT NULL, \
							`message` VARCHAR(1024) NOT NULL COLLATE 'cp1251_general_ci', \
							`add_time` DATETIME NOT NULL, \
							`category` SMALLINT(5) UNSIGNED NOT NULL \
						) \
						COLLATE='cp1251_general_ci' \
						ENGINE=MyISAM \
						;"
					,                                 # копируем в таблицу данные начиная с предыдущих суток
						'INSERT INTO `inc_diagnosis_journal` SELECT * FROM `diagnosis_journal` \
						WHERE to_days(add_time)<=to_days(now()) and to_days(add_time)>to_days(now())-2;'
					,                                 # создаем таблицу для сохранения инкрементальных данных
						"CREATE TABLE `inc_alarms` ( \
							`al_id` BIGINT(20) UNSIGNED NOT NULL, \
							`al_dt` DATETIME NOT NULL, \
							`al_obj` BIGINT(20) NOT NULL, \
							`al_param` BIGINT(20) NOT NULL, \
							`al_value` FLOAT(12) NULL DEFAULT NULL, \
							`alarm` TINYINT(4) NOT NULL DEFAULT '0', \
							`al_user_approved` BIGINT(20) NULL DEFAULT NULL, \
							`al_date_approved` DATETIME NULL DEFAULT NULL, \
							`al_message` VARCHAR(194) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' \
						) \
						COLLATE='utf8_unicode_ci' \
						;"
					,                                 # копируем в таблицу данные начиная с предыдущих суток
						'INSERT INTO `inc_alarms` SELECT * FROM `alarms` \
						WHERE to_days(al_dt)<=to_days(now()) and to_days(al_dt)>to_days(now())-2;'
					,
				],
				'AFTER_BACKUP_SQL': [                 # SQL скрипты выполняемые после резервного копирования
					                                  # удаляем таблицы с инкрементальными данными
					'DROP TABLE IF EXISTS `inc_raw`;',
					'DROP TABLE IF EXISTS  `inc_diagnosis_journal` ;',
					'DROP TABLE IF EXISTS  `inc_alarms`;',
				],
				'AFTER_RESTORE_SQL': [                # SQL скрипты выполняемые после резервного копирования
					                                  # удаляем избыточные данные
					'DELETE FROM inc_raw WHERE rid<=(SELECT MAX(rid) FROM raw);',
					'DELETE FROM inc_diagnosis_journal WHERE id<=(SELECT MAX(id) FROM diagnosis_journal);',
					'DELETE FROM inc_alarms WHERE al_id<=(SELECT MAX(al_id) FROM alarms);',
					                                  # востанавличаем инкрементальные данные
					'INSERT INTO `alarms` SELECT * FROM `inc_alarms`;',
					'INSERT INTO `diagnosis_journal` SELECT * FROM `inc_diagnosis_journal`;',
					'INSERT INTO `raw` SELECT * FROM `inc_raw`;'
					                                  # удаляем таблицы с инкрементальными данными
					'DROP TABLE IF EXISTS `inc_raw`;',
					'DROP TABLE IF EXISTS  `inc_diagnosis_journal` ;',
					'DROP TABLE IF EXISTS  `inc_alarms`;',
				],
				'DUMP_TABLES': [                      # резервировать указанные таблицы (маска указывается в формате как для LIKE)
					'%',
				],
				'SKIP_TABLES': [                      # исключения (для каких таблиц не производить резервное копирование)
					'%tempdump',
					                                  # пропускаем таблицы с которых мы берем инкрементальные данные
					'raw',
					'diagnosis_journal',
					'alarms',
				],
			},
			'newskz': {
				'DB_USER': 'skz',
				'DB_PASSWORD': 'superskz',
				'PART_SIZE': 100,                     # размер в мегабайтах для деления таблицы на части
				'DROP_TABLES': True,                  # удалять таблицы перед восстановлением
				'PREV_BACKUP_SQL': [                  # SQL скрипты выполняемые перед резервным копированием
						'DROP TABLE IF EXISTS `inc_alarms`;'
					,
						"CREATE TABLE `inc_alarms` ( \
							`id` INT(11) NOT NULL, \
							`skz_id` SMALLINT(5) UNSIGNED NOT NULL, \
							`add_date` DATETIME NOT NULL, \
							`type` SMALLINT(6) NOT NULL, \
							`alarm` SMALLINT(6) NOT NULL, \
							`org_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL, \
							`confirmed_by_id` INT(11) NULL DEFAULT NULL, \
							`confirmed_at` DATETIME NULL DEFAULT NULL, \
							`message` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' \
						) \
						COLLATE='utf8_general_ci' \
						;"
					,
						'INSERT INTO `inc_alarms` SELECT * FROM `alarms` \
						WHERE to_days(add_date)<to_days(now()) and to_days(add_date)>to_days(now())-2;'
					,
						'DROP TABLE IF EXISTS `inc_parameters`;'
					,
						"CREATE TABLE `inc_parameters` ( \
							`id` INT(11) NOT NULL, \
							`skz_id` SMALLINT(5) UNSIGNED NOT NULL, \
							`voltage` DOUBLE(22,0) NULL DEFAULT NULL, \
							`current` DOUBLE(22,0) NULL DEFAULT NULL, \
							`differ` DOUBLE(22,0) NULL DEFAULT NULL, \
							`polar` DOUBLE(22,0) NULL DEFAULT NULL, \
							`temperature` SMALLINT(6) NULL DEFAULT NULL, \
							`door` TINYINT(1) NOT NULL, \
							`electric_meter` DOUBLE(22,0) NULL DEFAULT NULL, \
							`state` SMALLINT(5) UNSIGNED NULL DEFAULT NULL, \
							`mode` SMALLINT(5) UNSIGNED NULL DEFAULT NULL, \
							`add_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', \
							`is_manual` TINYINT(1) NULL DEFAULT '0' \
						) \
						COLLATE='utf8_general_ci' \
						;"
					,
						'INSERT INTO `inc_parameters` SELECT * FROM `parameters` \
						WHERE to_days(add_date)<to_days(now()) and to_days(add_date)>to_days(now())-2;'
					,
				],
				'AFTER_BACKUP_SQL': [              # SQL скрипты выполняемые после резервного копирования
					'DROP TABLE IF EXISTS  `inc_alarms` ;',
					'DROP TABLE IF EXISTS  `inc_parameters` ;',
				],
				'AFTER_RESTORE_SQL': [             # SQL скрипты выполняемые после резервного копирования
					                               # удаляем избыточные данные
					'delete FROM inc_parameters WHERE id=(SELECT MAX(id) FROM parameters);',
					'delete FROM inc_alarms WHERE id=(SELECT MAX(id) FROM alarms);',
					                               # восстанавливаем икрементальные данные
					'INSERT INTO `alarms` SELECT * FROM `inc_alarms`;',
					'INSERT INTO `parameters` SELECT * FROM `inc_parameters`;',
					                               # удаляем временные таблицы
					'DROP TABLE IF EXISTS  `inc_alarms`;',
					'DROP TABLE IF EXISTS `inc_parameters`;',
				],
				'DUMP_TABLES': [                   # резервировать указанные таблицы
					'%',
				],
				'SKIP_TABLES': [                   # исключения (для каких таблиц не производить резервное копирование)
					'%tempdump',
					'parameters',
					'alarms',
				],
			},
		},
	},
	                                               # можно задать параметры для второго и следующих серверов
]
