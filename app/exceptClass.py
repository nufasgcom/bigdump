#: -*- coding: utf-8 -*-
"""
 Класс исключения при дампе БД
"""


class DumpError(Exception):
	def __init__(self, message=""):
		self.message = message
		# переопределяется конструктор встроенного класса `Exception()`
		super(DumpError, self).__init__('Во время резервного копирования произошла ошибка. Продолжение невозможно ->'+self.message)