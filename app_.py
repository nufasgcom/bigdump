#: -*- coding: utf-8 -*-
"""
Приложение для резервного копирования БД MYSQL.
Упор сделан на резервирование больших таблиц во время работы БД.
Идея следующая:
	1. Резервное копирование больших таблиц производится n частями (select * from table limit x,y).
	2. Для обеспечения ссылочной целостности сначала копируются таблицы которые ссылаются на другие таблицы.
	3. Добавлена возможность создания инкрементальных бакапов (необходимо дописать SQL скрипты в конфигурации для необходмых таблиц)
	4. Конфигурация находится в файле app/config.py (есть комментарии)
"""
from __future__ import print_function
import MySQLdb
from app import config
from app.exceptClass import DumpError
import logging
from logging.handlers import RotatingFileHandler
from app.cfgClass import CFG
import datetime
import time
import os
import zipfile

sfh = 0
fh = 0
err_cnt = 0

def createPath(path):
	try:
		os.makedirs(path)
	except OSError:
		log.error('Не удалось создать путь для записи дампа: "{path}"'.format(path=path))
		return False
	else:
		return True


def saveToFile(file_, text):
	try:
		file_.write(text)
	except Exception:
		log.error('Ошибка записи в файл "{file}" текста : "{text}"'.format(file=file_, text=text))
		return False
	else:
		return True

def getHeaderScript():
	script = addstr("/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;")
	script += addstr("/*!40101 SET NAMES utf8 */;")
	script += addstr("/*!50503 SET NAMES utf8mb4 */;")
	script += addstr("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;")
	script += addstr("/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;")
	return script

def getFooterScript():
	script = addstr("/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;")
	script += addstr("/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;")
	script += addstr("/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;")
	return script


def dumpExecuter():  # Главная процедура запуска резервного копирования
	global fh
	global err_cnt
	global log
	print("___________________________________________________________________________")
	err_cnt = 0      # количество ошибок во время дампа
	for conf in config.configs:
		log.info('Чтение конфигурации')
		#print('Чтение конфигурации')
		cfg = CFG(conf)
		if not os.path.exists(cfg.dir):
			if not createPath(cfg.dir):
				raise DumpError('Сan`t create a folder: {}'.format(cfg.dir))
		log = getLogger(cfg.logfile)
		log.info('Создаем/Открываем файл "{file}" для записи дампа'.format(file=cfg.sqlfile))
		#print('Создаем/Открываем файл "{file}" для записи дампа'.format(file=cfg.sqlfile))
		try:
			dumpfile = open(cfg.sqlfile, 'a')
		except OSError:
			err_cnt += 1
			log.error('Не удалось создать/открыть файл для записи дампа: "{file}"'.format(file=cfg.sqlfile))
			#print('Не удалось создать/открыть файл для записи дампа: "{file}"'.format(file=cfg.sqlfile))
			raise DumpError('Сan`t create file: {}'.format(cfg.sqlfile))
		script = getHeaderScript()
		saveToFile(dumpfile, script)
		for param in cfg.dbparams:
			try:
				conn = MySQLdb.connect(host=cfg.host, port=cfg.port, user=param.user, passwd=param.password, db=param.db)
			except MySQLdb.Error as e:
				err_cnt += 1
				err = "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
				log.error('Произошла ошибка при подключении к БД: {err}'.format(err=err))
				#print('Произошла ошибка при подключении к БД: {err}'.format(err=err))
			else:  # если нет ошибок
				dumpDB(conn, cfg, param, dumpfile)  # Перейти к дампу БД
				log.info(
					"Закрываем подключение к серверу:'{host}' на порт:'{port}'".format(host=cfg.host, port=cfg.port))
				log.info("к БД:'{db}' пользователем:'{user}'".format(db=param.db, user=param.user))
				conn.close()
		script = getFooterScript()
		saveToFile(dumpfile, script)
		dumpfile.close()
		if err_cnt == 0:
			log.info('Резервирование завершено без ошибок')
			#print('Резервирование завершено без ошибок')
		else:
			log.error('Во время резервирования возникло {} ошибок'.format(err_cnt))
			#print('Во время резервирования возникло {} ошибок'.format(err_cnt))
		fh.flush()
		fh.close()
	sfh.flush()
	sfh.close()


	# Архивирование

	dumptoarc([cfg.sqlfile, cfg.logfile], cfg.arcfile, True)
	cfg.removeOldFiles()

def dumptoarc(infiles, arcfile, DeleteInFiles=False):
	print('Архивирование файлов:')
	time.sleep(0.1)
	zip = zipfile.ZipFile(arcfile, 'w')
	for infile in infiles:
		print(infile)
		zip.write(infile)
		os.remove(infile)
	zip.close()


def getLogger(file_name):  # Инициализация логгера
	global fh
	global log
	log_formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
	#
	# logger = logging.getLogger(file_name)
	# logger.setLevel(logging.INFO)

	# create the logging file handler
	fh = logging.FileHandler(file_name)

	formatter = logging.Formatter(log_formatter)
	fh.setFormatter(formatter)

	# add handler to logger object
	log.addHandler(fh)
	return log


def getSysLogger():  # Инициализация логгера
	global sfh
	logger = logging.getLogger("BigDumper")
	logger.setLevel(logging.INFO)
	# create the logging file handler
	sfh = RotatingFileHandler("BigDumper.log", mode='a', maxBytes=10 * 1024 * 1024,
	                      backupCount=1, encoding=None, delay=0)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s  - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s')
	sfh.setFormatter(formatter)

	console_out = logging.StreamHandler()

	# add handler to logger object
	logger.addHandler(sfh)
	logger.addHandler(console_out)
	return logger

def addstr(str):
	return str+'\r\n'

def getInsertScript(table, data, cur, valNoSpace=False, insertOneLine=False, dumpfile=None):
	script = ''
	if len(data) > 0:
		if valNoSpace:  # Ставить или нет пробел между значениями
			space_char = ''
		else:
			space_char = ' '
		script += addstr('INSERT INTO `{table_name}`').format(table_name=table)
		field_descr = cur.description
		fields = ''
		delim = ''
		for col in field_descr:    # формируем список полей
			field_name = col[0]     # получаем имя поля
			fields += "{delim}{space}`{field}`".format(delim=delim, field=field_name, space=space_char)
			delim = ','
		script += addstr('   ({fields})'.format(fields=fields))
		script += addstr('VALUES')
		if not(dumpfile is None):
			saveToFile(dumpfile, script)
		row_delim = ''
		if not (dumpfile is None):
			script = ''
		for row in data:
			if not (dumpfile is None):
				script = ''
			row_data = ''
			col_delim = ''
			for val in row:
				if isinstance(val, str):
					val = val.replace("\r", "\\r")      # Экранирую символы переноса строки
					val = val.replace("\n", "\\n")      # Экранирую символы переноса строки
					if val.find('"') == -1:       # при наличии одинарных кавычек в тексте
						col_data = '"{val}"'.format(val=val)        # Вставим текст в двойные кавычки
					else:                                           # иначе
						col_data = "'{val}'".format(val=val)        # Вставим текст в одинарные кавычки
				elif isinstance(val, datetime.date) or isinstance(val, datetime.datetime) or isinstance(val, datetime.time):
					val = str(val)         # Преобразование в строковые данные
					col_data = '"{val}"'.format(val=val)  # Вставим текст в двойные кавычки
				else:
					col_data = str(val)  # Преобразование в строковые данные
				if col_data == 'None':   # Преобразуем None в NULL
					col_data = 'NULL'
				row_data += '{delim}{space}{column_data}'.format(delim=col_delim, column_data=col_data, space=space_char)
				col_delim = ','
			if insertOneLine:
				script += ('{delim}{space}({row_data})'.format(delim=row_delim, row_data=row_data, space=space_char))
			else:
				script += addstr('{delim}{space}({row_data})'.format(delim=row_delim, row_data=row_data, space=space_char))
			row_delim = ','

			if not (dumpfile is None):
				saveToFile(dumpfile, script)

		if not (dumpfile is None):
			saveToFile(dumpfile, addstr(';'))
		else:
			script = '{code};'.format(code=script)

	return script


def dumpDB(conn, cfg, param, dumpfile):  # Резервное копирование базы данных

	def dumpCreateTable(table_):        # Получаем и сохраняем скрипт для создания таблицы
		sql_ = 'SHOW CREATE TABLE `{db}`.`{table}`;'.format(db=param.db, table=table_)
		date_, cur_ = execQuery(sql_, 'Ошибка получения скрипта создания таблицы: "{sql}" с ошибкой'.format(sql=sql_))
		if len(date_) <= 0:
			return False
		else:
			script_ = addstr('{script};'.format(script=date_[0][1]))
			if not saveToFile(dumpfile, script_):
				return False
			return True

	def execQuery(sql_, desc_):
		global err_cnt
		result = ()
		try:
			query.execute(sql_)
		except MySQLdb.Error as e:
			err_cnt += 1
			err = "Err№ {%i}: MySQL Error [%d]: %s" % (err_cnt, e.args[0], e.args[1])
			log.error(
				'{err}. \r\n Не удалось выполнить: {err_desc} -> \r\n {sql} \r\n'.format(err=err, err_desc=desc_, sql=sql_))
			#print (err)
		else:
			result = query.fetchall()
		return result, query

	def dumpTableData(table_):  # Получаем и сохраняем данные из таблицы
		desc_ = 'Получение информации о таблице:"{table}"'.format(table=table_)
		log.info(desc_)
		sql_ = 'select table_rows rows_count, data_length length ' \
			'from information_schema.tables t ' \
			'where t.TABLE_SCHEMA = "{db}" and t.TABLE_NAME = "{table}"'.format(db=param.db, table=table_)
		data_, cur_ = execQuery(sql_, desc_)
		if len(data_) > 0:
			row_cnt = data_[0][0]
			size = data_[0][1] / (1024 * 1024)  # Размер в мегабайтах
			if size > param.part_size:  # Если размер данных таблицы больше максимального блока для записи
				row_part_cnt = int(row_cnt//(size/param.part_size))    # то количество строк для одного блока
			else:
				row_part_cnt = row_cnt    # иначе все записи в один блок
			#print('{cnt} записей, блоками по {part_cnt} записей'.format(cnt=row_cnt, part_cnt=row_part_cnt))
			row_exp_cnt = 0     # Количество отобранных записей
			script_ = ''
			desc_ = 'Получение данных из таблицы:"{table}" {cnt} записей'.format(table=table_, cnt=row_cnt)
			log.info(desc_)
			one_percent_count = row_cnt/100.0
			prev_delta=0
			while row_exp_cnt < row_cnt:    # Цикл по всем записям
				if one_percent_count > 0:   # Если есть записи
					percent = (row_exp_cnt+row_part_cnt)/one_percent_count  # Подсчитываем кол-во процентов под выгрузку на текушем шаге
					percent_step = 1   # Шаг для вывода на экран в процентах
					delta = int((percent + 1) / percent_step)
					if (delta-prev_delta) >= 1 or prev_delta == 0:
						if delta > 100:
							delta = 100
						#print('#'*(delta-prev_delta), end='')
						prev_delta = delta
				desc_ = 'Получение из таблицы:"{table}" начиная с {bgn} по {end}'.format(table=table_, bgn=row_exp_cnt, end=(row_exp_cnt+row_part_cnt-1))
				log.info(desc_)
				sql_ = "select * from {table} limit {bgn},{cnt}".format(table=table_, bgn=row_exp_cnt, cnt=row_part_cnt)
				data_, cur_ = execQuery(sql_, sql_)
				saveToFile(dumpfile, addstr('-- Вставка значений с {bgn} записи по {end} запись'.format(bgn=row_exp_cnt, end=(row_exp_cnt+row_part_cnt-1))))
				row_exp_cnt += row_part_cnt
				if cfg.saveByLine:
					log.info('Формирование и запись скрипта')
					script_ = addstr(getInsertScript(table_, data_, cur_, cfg.insertNoSpace, cfg.insertOneLine, dumpfile))
				else:
					log.info('Формирование скрипта')
					script_ = addstr(getInsertScript(table_, data_, cur_, cfg.insertNoSpace, cfg.insertOneLine))
					log.info('Запись данных в файл')
					if not saveToFile(dumpfile, script_):
						#print('')   # Для начала вывода на следующую строку
						return False
			#print('')   # Для начала вывода на следующую строку
			return True
		else:
			return False

	def dumpTable(table_):
		try:
			log.info('Начало резервирования таблицы: "{table}"'.format(table=table_))
			if not dumpCreateTable(table_):
				raise DumpError('Не удалось получить скрипт создания таблицы: "{table}"'.format(table=table_))
			if not dumpTableData(table_):
				raise DumpError('Не удалось выполнить резервирование таблицы: "{table}"'.format(table=table_))
			log.info('Конец резервирования таблицы: "{table}"'.format(table=table_))
		except DumpError as e:
			err = "%s" % (e.args[0])
			desc_ = 'Ошибка резервирования таблицы: "{table}. {err}"'.format(err=err, table=table_)
			log.error(desc_)
			#print(desc_)

	def getRefTables(table_):  # Получаем ссылки на таблицы
		log.debug('Выборка ссылок на таблицы')
		sql_ = "select REFERENCED_TABLE_NAME from information_schema.KEY_COLUMN_USAGE kcu where " \
			"kcu.TABLE_SCHEMA = '{db}' and kcu.TABLE_NAME = '{table}' " \
			"and kcu.REFERENCED_TABLE_SCHEMA = '{db}' and kcu.TABLE_NAME<>'' " \
			"order by kcu.ORDINAL_POSITION".format(db=param.db, table=table_)
		result, cur_ = execQuery(sql_,
			'Неудалось выполнить выборку ссылок на таблицы для таблицы {db}.{table}'.format(db=param.db, table=table_))
		return result

	'''
		Сортировка таблиц для обеспечения ссылочной целостности
		в конец списка попадают таблицы без ссылок на другие таблицы
	'''

	def orderByReferences(data_):
		log.debug('Сортировка таблиц для обеспечения ссылочной целостности')
		tables_ = []
		for line in data_:  # Перевод в одномерный массив
			tables_.append(line[0])
		orderTables = []  # Результирующий массив отсортированных таблиц
		referTables = {}  # Справочник таблиц на который ссылается исходная таблица
		tables_copy = []
		tables_copy.extend(tables_)  # Делаем дубликат а то следующий цикл работает криво при удалении элемента из списка
		for table_ in tables_copy:
			arr = getRefTables(table_)  # Получили связанные таблицы
			if len(arr) == 0:  # Если нет связанных таблиц то переносим таблицу в сортированный список
				orderTables.append(table_)
				tables_.remove(table_)
			else:
				referTables[table_] = arr  # Заполняем справочник
		prev_cnt = 0
		while len(tables_) != 0 and prev_cnt != len(tables_):  # Пока есть неразобранные таблицы (prevcnt>tables_.count защита от зацикливания)
			prev_cnt = len(tables_)  # количество таблиц на входе
			tables_copy = []
			tables_copy.extend(tables_)  # Делаем дубликат а то следующий цикл работает криво при удалении элемента из списка
			for table_ in tables_:  # Пройдемся по оставшимся таблицам
				inOrder = True
				if referTables.has_key(table_):
					for referTable in referTables[table_]:  # Проверка находятся ли ссылочные таблицы в выходном списке
						if orderTables.count(referTable[0]) == 0:
							inOrder = False
				if inOrder:  # Если все ссылочные таблицы в выходном списке то можно переносить и таблицу table_ в выходной  список
					orderTables.append(table_)
					tables_.remove(table_)
		if len(tables_) > 0:
			log.warning(
				'Не удалось отсортировать все таблицы для обеспечения целостности. Оставшиеся таблицы добавлены в список первыми')
			orderTables.extend(tables_)
		orderTables.reverse()  # Для получения нужного нам порядка развернем список
		return orderTables

	def getTables():
		tables_ = ()
		desc_ = 'Получение списка таблиц из БД:"{db}"'.format(db=param.db)
		log.info(desc_)
		sql_ = 'select table_name from information_schema.tables where table_schema = "{dbname}"  and table_type = "BASE TABLE"'.format(
			dbname=param.db)
		for mask in param.dump_tables:  # брать таблицы соответствующие маскам param.dump_tables
			sql_ += ' and table_name like "{mask}"'.format(mask=mask)
		for mask in param.skip_tables:  # пропускать таблицы соответствующие маскам param.skip_tables
			sql_ += ' and table_name not like "{mask}"'.format(mask=mask)
		data_, cur_ = execQuery(sql_, desc_)
		if data_.count != 0:
			tables_ = orderByReferences(data_)
		return tables_

	def dumpTables():   # Резервное копирование таблиц
		tables = getTables()
		for table in tables:
			log.info('Сохранение таблицы: {table}.'.format(table=table))
			dumpTable(table)

	def getViews():
		sql_ = "select v.TABLE_NAME from information_schema.views v where v.TABLE_SCHEMA='{db}'".format(db=param.db)
		desc_ = "Получение списка представлений"
		data_, cur_ = execQuery(sql_, desc_)
		views = []
		for row in data_:
			view = row[0]
			views.append(view)
		return views

	def dumpView(view):
		sql_ = 'SHOW CREATE VIEW `{db}`.`{view}`;'.format(db=param.db, view=view)
		desc_ = 'Получения кода создания представления {db}.{view}'.format(db=param.db, view=view)
		data_, cur_ = execQuery(sql_, desc_)
		delim = cfg.procDelimetr
		script_ = addstr('DELIMITER {delim}').format(delim=delim)
		script_ += addstr((data_[0][1]).strip())
		script_ += addstr(delim)
		saveToFile(dumpfile, script_)

	def dumpViews():    # Резервное копирование вьюшек
		desc_ = 'Резервное копирование представлений';
		log.info(desc_)
		#print(desc_)
		views = getViews()      # Получаем названия вьюшек
		for view in views:
			dumpView(view)      # Резервируем представление

	def getTriggers():
		desc_ = 'Получение списка триггеров'
		log.info(desc_)
		#print(desc_)
		sql_ = "select t.TRIGGER_NAME from information_schema.`TRIGGERS` t where t.TRIGGER_SCHEMA='{db}';".format(db=param.db)
		data_, cur_ = execQuery(sql_, desc_)
		triggers = []
		for row_ in data_:
			trigger = row_[0]
			triggers.append(trigger)
		return triggers

	def dumpTrigger(trigger):   # Резервное копирование тригера
		desc_ = 'Получение кода создания триггера {db}.{trigger}'.format(db=param.db, trigger=trigger)
		log.info(desc_)
		#print(desc_)
		sql_ = 'show create trigger `{db}`.`{trigger}`;'.format(db=param.db, trigger=trigger)
		data_, cur_ = execQuery(sql_, trigger)
		delim = cfg.procDelimetr
		script_ = addstr('DELIMITER {delim}').format(delim=delim)
		script_ += addstr((data_[0][2]).strip())
		script_ += addstr(delim)
		saveToFile(dumpfile, script_)

	def dumpTriggers():     # Резервное копирование тригиров
		desc_ = 'Резервное копирование тригиров'
		log.info(desc_)
		#print(desc_)
		triggers = getTriggers()
		for trigger in triggers:
			dumpTrigger(trigger)

	def getProcs():
		sql_ = "select r.ROUTINE_NAME, r.ROUTINE_TYPE from information_schema.ROUTINES r where r.ROUTINE_SCHEMA = '{db}'".format(db=param.db)
		desc_ = "Получаем список процедур из БД {db}".format(db=param.db)
		data_, cur_ = execQuery(sql_, desc_)
		procs = []
		for row_ in data_:
			procs.append(row_[0])
		return procs

	def dumpProc(proc):     # Резервирование процедуры
		sql_ = "SHOW CREATE PROCEDURE `{db}`.`{proc}`;".format(db=param.db, proc=proc)
		desc_ = 'Получение кода процедуры `{db}`.`{proc}`'.format(db=param.db, proc=proc)
		data_, cur_ = execQuery(sql_, desc_)
		delim = cfg.procDelimetr
		print(cur_);
		print(sql_);
		print(data_);
		print(data_[0][2]);
		script_ = addstr('DELIMITER {delim}').format(delim=delim)
		script_ += addstr((data_[0][2]).strip())
		script_ += addstr(delim)
		saveToFile(dumpfile, script_)

	def dumpProcs():     # Копирование процедур
		desc_ = 'Резервное копирование процедур'
		log.info(desc_)
		#print(desc_)
		procs = getProcs()      # Получаем названия процедур
		for proc in procs:
			dumpProc(proc)


	def dumpUserSQL():     # копирование скриптов SQL которые необходимо выполнить после восстановления БД
		desc_ = 'Копирование скриптов завершения востановления БД'
		log.info(desc_)
		#print(desc_)
		scripts = param.after_backup_sql     # Получаем скрипты
		for script in scripts:
			saveToFile(dumpfile, script)


	def runScripts(scripts):
		desc_ = 'Выполнение скриптов (перед/после резервирования)'
		log.info(desc_)
		#print(desc_)
		for script in scripts:
			log.info(script)
			#print(script)
			execQuery(script, script)


	def createDB():     # Создаем БД
		sql_ = 'SHOW CREATE DATABASE `{db}`;'.format(db=param.db)
		desc_ = 'Ошибка получения скрипта создания БД "{db}" запросу : {sql}.'.format(db=param.db, sql=sql_)
		data_, cur_ = execQuery(sql_, desc_)
		if len(data_) == 0:
			raise DumpError('No data to create DB: {}'.format(param.db))
		script_ = addstr('-- Backup {db} from {dt}'.format(db=param.db, dt=cfg.backup_dt_str))
		script_ += addstr('{script};'.format(script=data_[0][1]))      # Забираем код создания из первой строки первого столбца
		script_ += addstr('use `{db}`;'.format(db=param.db))
		#print (script_)
		if not saveToFile(dumpfile, script_):
			raise DumpError('Can`t save script to file (Create DB)')

	# ---dumpDB body---------------------------------------------------------------------------------------
	begin_err_cnt = err_cnt
	query = conn.cursor()
	print('Начало резервного копирования БД:"{db}"'.format(db=param.db))
	log.info('Начало резервного копирования БД:"{db}"'.format(db=param.db))
	#print('Начало резервного копирования БД:"{db}"'.format(db=param.db))
	# Формирование скрипта создания дампа
	#try:
#	runScripts(param.prev_backup_sql)      # выполняем SQL до бакапа
#	createDB()      # Создание и подключение к БД
#	dumpTables()    # Резервирование таблиц
	dumpProcs()     # Резервирование  процедур и функций
	dumpViews()     # Резервирование представлений
	dumpTriggers()  # Резервирование триггеров
#	dumpUserSQL()   # Сохраняем SQL который нужно выполнить после восстановления бд
#	runScripts(param.after_backup_sql)     # выполняем SQL после бакапа
	#except DumpError as e:
	#	print (e)
	#	print('При выполнении резервного копирования возникали ошибки')
	#	log.error('При выполнении резервного копирования возникали ошибки')
	#else:
	if (begin_err_cnt == err_cnt):
		print('Успешное завершение резервного копирования БД:"{db}"'.format(db=param.db))
		log.info('Успешное завершение резервного копирования БД:"{db}"'.format(db=param.db))
	else:
		print('При выполнении резервного копирования возникали ошибки')
		log.error('При выполнении резервного копирования возникали ошибки')


if __name__ == '__main__':
	log = getSysLogger()
	#log = syslog
	dumpExecuter()
